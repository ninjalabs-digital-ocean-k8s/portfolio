<script>
    export default {
        meta: {
            title: 'Welcome to my Blog!',
            description: 'This is the first article.',
            image: '/img/blog/hello.svg'
        }
    }
</script>

# Welcome to my Blog!
![waving hand](/img/blog/hello.svg)
Welcome to my blog, I will try to write something from time to time.

I don't know what I'm going to publish here, but I think it will most likely be something about Docker, Laravel, Feathers or Vue, because that are Frameworks I use regularly.
